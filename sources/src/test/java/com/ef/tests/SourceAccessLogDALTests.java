package com.ef.tests;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ef.Parser;
import com.ef.dal.SourceAccessLogDAL;
import com.ef.dal.entities.AccessLogEntity;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes={Parser.class},
		properties={
				"startDate=2017-01-01.13:00:00", 
				"duration=hourly", 
				"accesslog=src/test/resources/access.log", 
				"threshold=100"})
public class SourceAccessLogDALTests
{
	@Autowired
	private SourceAccessLogDAL dal;
	
	@Test
	public void testBatchIteratorCount() {
		Assert.assertEquals(13, fetchLog().size());
	}

	@Test
	public void testBatchIteratorUserAgentCounts()
	{
		long swcdCount = fetchLog()
				.stream()
				.filter(e -> e.getUserAgent()
				.contains("swcd"))
				.count();
		
		Assert.assertEquals(6, swcdCount);
	}
	
	private List<AccessLogEntity> fetchLog() {
		List<AccessLogEntity> list = new ArrayList<AccessLogEntity>();
		
		Iterator<List<AccessLogEntity>> iterator = dal.batchIterator(3);
		while (iterator.hasNext()) {
			list.addAll(iterator.next());
		}
		return list;
	}
}
