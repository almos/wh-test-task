package com.ef.tests;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ef.Parser;
import com.ef.dal.DestinationAccessLogDAL;
import com.ef.dal.ImportMetadataDAL;
import com.ef.dal.entities.ImportMetadataEntity;
import com.ef.dal.entities.ThresholdingIpEntity;
import com.ef.service.ImportService;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes={Parser.class},
		properties={
				"startDate=2017-01-01.00:00:00", 
				"duration=hourly", 
				"accesslog=src/test/resources/access.log", 
				"threshold=1"})
public class DestinationAccessLogDALTests
{
	@Autowired
	private DestinationAccessLogDAL accesslogDal;
	
	@Autowired
	private ImportMetadataDAL metadataDal;
	
	@Autowired
	private ImportService importService;
	
	@Test
	public void testMetadataInsertion()
	{
		ImportMetadataEntity me1 = metadataDal.insert(new ImportMetadataEntity("test1.sql")),
				me2 = metadataDal.insert(new ImportMetadataEntity("test2.sql")),
				me3 = metadataDal.insert(new ImportMetadataEntity("test3.sql"));
		
		Assert.assertEquals(1, me1.getImportId());
		Assert.assertEquals(2, me2.getImportId());
		Assert.assertEquals(3, me3.getImportId());
		
		Assert.assertTrue(metadataDal.exists(me1.getImportId()));
		Assert.assertTrue(metadataDal.exists(me2.getImportId()));
		Assert.assertTrue(metadataDal.exists(me3.getImportId()));
	}
	
	@Test
	public void testThresholdingIP()
	{
		List<ThresholdingIpEntity> thresholds = importService.trackThresholds(
				importService.importData());

		List<String> ipAddresses = thresholds
			.stream()
			.map(e -> e.getIp())
			.collect(Collectors.toList());
		
		Assert.assertEquals(2, ipAddresses.size());
		Assert.assertTrue(ipAddresses.containsAll(Arrays.asList("192.168.234.82", "192.168.169.194")));
	}
}
