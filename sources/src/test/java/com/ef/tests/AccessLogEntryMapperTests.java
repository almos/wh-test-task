package com.ef.tests;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ef.Parser;
import com.ef.dal.entities.AccessLogEntity;
import com.ef.mapper.AccessLogEntryMapper;
import com.ef.utils.DateUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes={Parser.class},
		properties={
				"startDate=2017-01-01.13:00:00", 
				"duration=hourly", 
				"accesslog=src/test/resources/access.log", 
				"threshold=100"})
public class AccessLogEntryMapperTests
{
	@Autowired
	private AccessLogEntryMapper mapper;
	
	@Test
	public void testMapperForNullInput() {
		Assert.assertEquals(null, mapper.map(null));
	}
	
	@Test
	public void testMapperForWrongFieldsNumber() {
		Assert.assertEquals(null, mapper.map(new String[]{}));
	}
	
	@Test
	public void testMapperForValidInput() {
		AccessLogEntity entry = mapper.map(new String[]{
				"2017-01-01 00:00:11.763",
				"192.168.234.82",
				"GET / HTTP/1.1",
				"200",
				"libcurl"});

		LocalDateTime dt = LocalDateTime.of(2017, 1, 1, 0, 0, 11)
				.plus(763, ChronoUnit.MILLIS);
		
		Assert.assertNotNull(entry);
		Assert.assertEquals("libcurl", entry.getUserAgent());
		Assert.assertEquals(200, entry.getResponseCode());
		Assert.assertEquals("GET / HTTP/1.1", entry.getRequestLine());
		Assert.assertEquals(3232295506l, entry.getSourceIp());
		Assert.assertEquals(DateUtils.toMillis(dt), entry.getRequestTs());
	}
	
	@Test
	public void testInvalidDateTime() {
		AccessLogEntity entry = mapper.map(new String[]{
				"2017-01-01.00:00:11.763",
				"192.168.234.82",
				"GET / HTTP/1.1",
				"200",
				"libcurl"});

		Assert.assertNull(entry);
	}
	
	@Test
	public void testInvalidIp1() {
		AccessLogEntity entry = mapper.map(new String[]{
				"2017-01-01 00:00:11.763",
				"192.168.234.820",
				"GET / HTTP/1.1",
				"200",
				"libcurl"});

		Assert.assertNull(entry);
	}

	@Test
	public void testInvalidIp2() {
		AccessLogEntity entry = mapper.map(new String[]{
				"2017-01-01 00:00:11.763",
				"aaa",
				"GET / HTTP/1.1",
				"200",
				"libcurl"});

		Assert.assertNull(entry);
	}

	@Test
	public void testInvalidResponseCode() {
		AccessLogEntity entry = mapper.map(new String[]{
				"2017-01-01 00:00:11.763",
				"10.1.240.1",
				"GET / HTTP/1.1",
				"-1",
				"libcurl"});

		Assert.assertNull(entry);
	}
	
	@Test
	public void testNullTokens() {
		AccessLogEntity entry = mapper.map(new String[]{null, null, null, null, null});
		Assert.assertNull(entry);
	}
}
