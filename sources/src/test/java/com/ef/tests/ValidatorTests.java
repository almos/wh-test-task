package com.ef.tests;

import java.io.File;
import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

import com.ef.configuration.ApplicationConfiguration;
import com.ef.configuration.ApplicationConfiguration.DurationPeriod;
import com.ef.configuration.ConfigurationValidator;

@RunWith(SpringRunner.class)
@SpringBootTest(classes={ValidatorTests.class})
public class ValidatorTests
{
	private ConfigurationValidator validator;
	private File inputFile;
	
	@Before
	public void init() {
		validator = new ConfigurationValidator();
		inputFile = new File(this.getClass().getResource("/access.log").getFile());
	}
	
	@Test
	public void testValidatorSupportsApplicationConfigurationClass()
	{
		ConfigurationValidator validator = new ConfigurationValidator();
		Assert.assertEquals(true, validator.supports(ApplicationConfiguration.class));
	}
	
	@Test
	public void testAllEmptyConfigurationParameters()
	{
		ApplicationConfiguration config = createApplicationConfiguration(null, null, null, null);
		Errors mockedErrors = Mockito.mock(Errors.class);
		
		validator.validate(config, mockedErrors);
		
		verify(mockedErrors, times(1)).reject(matches("threshold"), contains("empty"));
		verify(mockedErrors, times(1)).reject(matches("duration"), contains("empty"));
		verify(mockedErrors, times(1)).reject(matches("startDate"), contains("empty"));
		verify(mockedErrors, times(1)).reject(matches("accessLogPath"), contains("empty"));
	}
	
	@Test
	public void testAllInvalidConfigurationParameters()
	{
		ApplicationConfiguration config = createApplicationConfiguration("/tmp/qwerty123z", "2017-01-01 13:00:00", "yearly", "100a");
		Errors mockedErrors = Mockito.mock(Errors.class);
		
		validator.validate(config, mockedErrors);
		
		verify(mockedErrors, times(1)).reject(matches("threshold"), contains("invalid"));
		verify(mockedErrors, times(1)).reject(matches("duration"), contains("invalid"));
		verify(mockedErrors, times(1)).reject(matches("startDate"), contains("invalid"));
		verify(mockedErrors, times(1)).reject(matches("accessLogPath"), contains("doesn't exist"));
	}
	
	@Test
	public void testNegativeThreshold()
	{
		Errors mockedErrors = Mockito.mock(Errors.class);
		ApplicationConfiguration config = createApplicationConfiguration(
				inputFile.getAbsolutePath(), "2017-01-01.13:00:00", "hourly", "-100");
		
		validator.validate(config, mockedErrors);
		
		verify(mockedErrors).reject(matches("threshold"), contains("greater than 0"));
	}
	
	@Test
	public void testAllValidConfigurationParameters()
	{
		Errors mockedErrors = Mockito.mock(Errors.class);
		ApplicationConfiguration config = createApplicationConfiguration(
				inputFile.getAbsolutePath(), "2017-01-01.13:00:00", "hourly", "100");
		
		validator.validate(config, mockedErrors);
		
		verify(mockedErrors, times(0)).reject(matches("threshold"), anyString());
		verify(mockedErrors, times(0)).reject(matches("duration"), anyString());
		verify(mockedErrors, times(0)).reject(matches("startDate"), anyString());
		verify(mockedErrors, times(0)).reject(matches("accessLogPath"), anyString());
	}
	
	public void testParsingOfValidConfigurationParameters()
	{
		ApplicationConfiguration config = createApplicationConfiguration(
				inputFile.getAbsolutePath(), "2017-01-01.13:00:00", "hourly", "100");
		
		config.init();
		
		Assert.assertEquals(DurationPeriod.HOURLY, config.getDuration());
		Assert.assertEquals(LocalDateTime.of(2017, 1, 1, 13, 0, 0), config.getStartDate());
		Assert.assertEquals(inputFile, config.getAccessLog());
		Assert.assertEquals(Integer.valueOf(100), config.getThreshold());
	}
	
	private ApplicationConfiguration createApplicationConfiguration(final String accessLog, final String startDate, 
			final String duration, final String threshold)
	{
		return new ApplicationConfiguration() {
			public ApplicationConfiguration construct() {
				setRawAccessLogPath(accessLog);
				setRawDuration(duration);
				setRawStartDate(startDate);
				setRawThreshold(threshold);
				return this;
			}
		}.construct();
	}
}
