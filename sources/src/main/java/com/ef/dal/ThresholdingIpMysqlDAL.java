package com.ef.dal;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.ef.dal.entities.ThresholdingIpEntity;
import com.ef.utils.DateUtils;

@Component
public class ThresholdingIpMysqlDAL implements ThresholdingIpDAL
{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /**
	 * Logger
	 */
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public void insert(List<ThresholdingIpEntity> entries)
	{
		String insertQuery = "INSERT INTO `hits` "
				+ "(h_i_id, h_ip, h_count, h_threshold, h_start, h_end, h_reason) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?) ";
		
		jdbcTemplate.batchUpdate(insertQuery,  
	        new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					ThresholdingIpEntity entry = entries.get(i);
					ps.setLong(1, entry.getImportId());
					ps.setString(2, entry.getIp());
					ps.setInt(3, (int) entry.getCount());
					ps.setInt(4, entry.getThreshold());
					ps.setTimestamp(5, new Timestamp(DateUtils.toMillis(entry.getStartDate())));
					ps.setTimestamp(6, new Timestamp(DateUtils.toMillis(entry.getEndDate())));
					ps.setString(7, entry.getReason());
				}

				public int getBatchSize() {
					return entries.size();
				}
			});
		
		log.info("Imported {} entries", entries.size());
	}
}
