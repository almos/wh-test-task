package com.ef.dal;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ef.configuration.ApplicationConfiguration;
import com.ef.dal.entities.AccessLogEntity;
import com.ef.mapper.AccessLogEntryMapper;
import com.ef.utils.FileUtils;
import com.opencsv.CSVReader;

@Component
public class SourceAccessLogFileDAL implements SourceAccessLogDAL
{
	@Autowired
	private ApplicationConfiguration config;
	
	@Autowired
	private AccessLogEntryMapper mapper;
	
	@Override
	public Iterator<AccessLogEntity> iterator() {
		return new FileBasedAccessLogIterator(config.getAccessLog());
	}
	
	@Override
	public Iterator<List<AccessLogEntity>> batchIterator(int batchSize) {
		return new FileBasedAccessLogBatchIterator(config.getAccessLog(), batchSize);
	}

	@SuppressWarnings("deprecation")
	class FileBasedAccessLogIterator implements Iterator<AccessLogEntity>
	{
		private CSVReader reader;
		private Iterator<String[]> iterator;
		
		public FileBasedAccessLogIterator(File f) {
			reader = new CSVReader(FileUtils.createBufferedReader(f),
					config.getSourceDataSeparator(),
					config.getSourceDataQuote());
			iterator = reader.iterator();
		}
		
		@Override
		public boolean hasNext()
		{
			boolean hasNext = iterator.hasNext();
			
			if (!hasNext)
				FileUtils.closeQuitely(reader);
			
			return hasNext;
		}
		
		@Override
		public AccessLogEntity next() {
			return mapper.map(iterator.next());
		}
	}
	
	class FileBasedAccessLogBatchIterator implements Iterator<List<AccessLogEntity>>
	{
		private int batchSize;
		private FileBasedAccessLogIterator iterator;

		public FileBasedAccessLogBatchIterator(File f, int batchSize) {
			this.batchSize = batchSize;
			iterator = new FileBasedAccessLogIterator(f);
		}
		
		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public List<AccessLogEntity> next()
		{
			List<AccessLogEntity> entries = new ArrayList<>(batchSize);
			while (iterator.hasNext())
			{
				AccessLogEntity entry = iterator.next();
				
				// skipping invalid entries (that didn't pass validation)
				if (entry == null) 
					continue;
				
				entries.add(entry);
				
				if (entries.size() == batchSize)
					break;
			}
			
			return entries;
		}
	}
}
