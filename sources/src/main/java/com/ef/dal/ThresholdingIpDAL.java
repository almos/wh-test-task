package com.ef.dal;

import java.util.List;

import com.ef.dal.entities.ThresholdingIpEntity;

/**
 * Defines interface for storing IPs that thresholded 
 * specific limits during import and associated information 
 */
public interface ThresholdingIpDAL
{
	/**
	 * Inserts batch of entries that met threshold criteria during import
	 * @param entries
	 */
	void insert(List<ThresholdingIpEntity> entries);
}
