package com.ef.dal;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import com.ef.dal.entities.AccessLogEntity;

/**
 * Defines interface for logging entries destination storage layer 
 */
public interface DestinationAccessLogDAL
{
	/**
	 * Inserts bulk of log entries
	 * @param entries list of entries to insert
	 * @param importId id of import activity to associate entries with
	 */
	void insert(List<AccessLogEntity> entries, long importId);
	
	/**
	 * Finds IPs which have interacted with server more than "threshold" during 
	 * time period between startDate and endDate
	 * @param startDate start time frame point
	 * @param endDate end time frame point
	 * @param threshold 
	 * @param importId id of import activity which data set needs to be checked
	 * @return Map where key is an IP and value is number of times IP interacted within time frame
	 */
	Map<String, Long> find(LocalDateTime startDate, LocalDateTime endDate, int threshold, long importId);
	
	/**
	 * Count number of imported entries for specific import activity
	 * @param importId
	 * @return
	 */
	long count(long importId);
}
