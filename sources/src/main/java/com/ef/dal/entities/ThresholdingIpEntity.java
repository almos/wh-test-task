package com.ef.dal.entities;

import java.time.LocalDateTime;

import com.ef.dal.ThresholdingIpDAL;

/**
 * Entity intended to be used with {@link ThresholdingIpDAL}
 * Represents information associated with IP addresses that exceeded 
 * interaction threshold to specific server instance 
 */
public class ThresholdingIpEntity
{
	/**
	 * IP address that exceeded interaction threshold
	 */
	private String ip;
	
	/**
	 * ID of import run
	 */
	private long importId;
	
	/**
	 * Number of IP interactions within
	 * the given timeframe
	 */
	private long count;
	
	/**
	 * Threshold value
	 */
	private int threshold;
	
	/**
	 * Start date of the given time frame
	 */
	private LocalDateTime startDate;
	
	/**
	 * End date of the given time frame
	 */
	private LocalDateTime endDate;
	
	/**
	 * Text representation of the reason IP 
	 * was classified as thresholded
	 */
	private String reason;
	
	public ThresholdingIpEntity() {
	}
	
	public String getIp() {
		return ip;
	}
	
	public ThresholdingIpEntity setIp(String ip) {
		this.ip = ip;
		return this;
	}
	
	public long getImportId() {
		return importId;
	}
	
	public ThresholdingIpEntity setImportId(long importId) {
		this.importId = importId;
		return this;
	}
	
	public long getCount() {
		return count;
	}
	
	public ThresholdingIpEntity setCount(long count) {
		this.count = count;
		return this;
	}
	
	public int getThreshold() {
		return threshold;
	}
	
	public ThresholdingIpEntity setThreshold(int threshold) {
		this.threshold = threshold;
		return this;
	}
	
	public LocalDateTime getStartDate() {
		return startDate;
	}
	
	public ThresholdingIpEntity setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
		return this;
	}
	
	public LocalDateTime getEndDate() {
		return endDate;
	}
	
	public ThresholdingIpEntity setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
		return this;
	}
	
	public String getReason() {
		return reason;
	}
	
	public ThresholdingIpEntity setReason(String reason) {
		this.reason = reason;
		return this;
	}
}
