package com.ef.dal.entities;

import com.ef.dal.ImportMetadataDAL;

/**
 * Entity intended to be used with {@link ImportMetadataDAL}
 * Import metadata allows to discriminate between different import runs 
 */
public class ImportMetadataEntity
{
	/**
	 * Import ID
	 */
	private long importId;
	
	/**
	 * Path to source file from which import was done
	 */
	private String sourceFileName;
	
	public ImportMetadataEntity(String sourceFileName) {
		this.setSourceFileName(sourceFileName);
	}

	public long getImportId() {
		return importId;
	}

	public void setImportId(long importId) {
		this.importId = importId;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
}
