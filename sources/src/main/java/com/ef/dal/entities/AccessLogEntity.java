package com.ef.dal.entities;

import com.ef.dal.DestinationAccessLogDAL;

/**
 * Entity intended to be used with {@link DestinationAccessLogDAL}
 */
public class AccessLogEntity
{
	/**
	 * Request date time with milliseconds
	 */
	private long requestTs;
	
	/**
	 * Source IP in numeric representation
	 */
	private long sourceIp;
	
	/**
	 * HTTP response code
	 */
	private int responseCode;
	
	/**
	 * HTTP request line
	 */
	private String requestLine;
	
	/**
	 * HTTP user agent
	 */
	private String userAgent;
	
	public long getRequestTs() {
		return requestTs;
	}
	
	public void setRequestTs(long requestTs) {
		this.requestTs = requestTs;
	}
	
	public long getSourceIp() {
		return sourceIp;
	}
	
	public void setSourceIp(long sourceIp) {
		this.sourceIp = sourceIp;
	}
	
	public int getResponseCode() {
		return responseCode;
	}
	
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	
	public String getRequestLine() {
		return requestLine;
	}
	
	public void setRequestLine(String requestLine) {
		this.requestLine = requestLine;
	}
	
	public String getUserAgent() {
		return userAgent;
	}
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
}
