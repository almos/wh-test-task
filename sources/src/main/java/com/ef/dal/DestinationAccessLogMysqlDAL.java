package com.ef.dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Component;

import com.ef.dal.entities.AccessLogEntity;
import com.ef.utils.DateUtils;
import com.ef.utils.NetworkUtils;

@Component
public class DestinationAccessLogMysqlDAL implements DestinationAccessLogDAL
{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /**
	 * Logger
	 */
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public void insert(List<AccessLogEntity> entries, long importId)
	{
		String insertQuery = "INSERT INTO `access_log` "
				+ "(al_ts, al_ip, al_rc, al_rl, al_ua, al_i_id) "
				+ "VALUES (?, ?, ?, ?, ?, ?) ";
		
		jdbcTemplate.batchUpdate(insertQuery,  
	        new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					AccessLogEntity entry = entries.get(i);
					ps.setLong(1, entry.getRequestTs());
					ps.setLong(2, entry.getSourceIp());
					ps.setInt(3, entry.getResponseCode());
					ps.setString(4, entry.getRequestLine());
					ps.setString(5,  entry.getUserAgent());
					ps.setLong(6, importId);
				}

				public int getBatchSize() {
					return entries.size();
				}
			});
		
		log.info("Imported {} entries", entries.size());
	}
	
	@Override
	public long count(long importId)
	{
		return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM `access_log` WHERE al_i_id = ?", 
				new Object[]{importId}, Long.class);
	}

	@Override
	public Map<String, Long> find(LocalDateTime startDate, LocalDateTime endDate, int threshold, long importId)
	{
		String sql = "SELECT al_ip, COUNT(*) "
				+ "FROM `access_log` "
				+ "WHERE al_i_id = ? "
				+ "AND (al_ts BETWEEN ? AND ?) "
				+ "GROUP BY al_ip "
				+ "HAVING COUNT(*) > ?";
		
		final Map<String, Long> result = new HashMap<>();
		RowCallbackHandler rowCallbackHandler = new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet r) throws SQLException {
				result.put(NetworkUtils.longToIp(r.getLong(1)), r.getLong(2));
			}
;		};
		
		jdbcTemplate.query(sql, new Object[]{importId, DateUtils.toMillis(startDate), 
				DateUtils.toMillis(endDate), threshold}, rowCallbackHandler);
		
		return result;
	}
}
