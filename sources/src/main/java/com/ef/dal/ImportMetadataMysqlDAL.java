package com.ef.dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Component;

import com.ef.dal.entities.ImportMetadataEntity;

@Component
public class ImportMetadataMysqlDAL implements ImportMetadataDAL
{
    @Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public ImportMetadataEntity insert(ImportMetadataEntity e)
	{
		String insertQuery = "INSERT INTO `imports` (i_ts, i_file) VALUES (now(), ?)";
		
		GeneratedKeyHolder holder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
		    @Override
		    public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
		        PreparedStatement statement = con.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
		        statement.setString(1, e.getSourceFileName());
		        return statement;
		    }
		}, holder);

		e.setImportId(holder.getKey().longValue());
		return e;
	}
	
	@Override
	public boolean exists(long id)
	{
		Integer count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM `imports` WHERE i_id = ?", 
				new Object[]{id}, Integer.class);
		
		return count == 1;
	}
}
