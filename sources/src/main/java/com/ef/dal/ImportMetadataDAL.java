package com.ef.dal;

import com.ef.dal.entities.ImportMetadataEntity;

/**
 * Defines interface for import metadata
 * Import metadata allow to discriminate between different import runs 
 */
public interface ImportMetadataDAL 
{
	/**
	 * Creates import metadata entry
	 * @param e
	 * @return
	 */
	ImportMetadataEntity insert(ImportMetadataEntity e);
	
	/**
	 * Checks whether import metadata with specific ID exists
	 * @param id
	 * @return
	 */
	boolean exists(long id);
}
