package com.ef.dal;

import java.util.Iterator;
import java.util.List;

import com.ef.dal.entities.AccessLogEntity;

/**
 * Defines interface for logging entries source storage layer 
 */
public interface SourceAccessLogDAL
{
	/**
	 * Provides iterator for entry-by-entry iteration from the source log data source
	 * @return
	 */
	Iterator<AccessLogEntity> iterator();
	
	/**
	 * Provides batch iterator for iteration from the source log data source
	 * which returns multiple records per iteration
	 * @param batchSize max number of entries per iteration
	 * @return
	 */
	Iterator<List<AccessLogEntity>> batchIterator(int batchSize);
}
