package com.ef;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.validation.Validator;

import com.ef.configuration.ConfigurationValidator;

@SpringBootApplication
@ComponentScan(basePackages="com.ef")
public class Parser
{
	public static void main(String[] args) {
		SpringApplication.run(Parser.class, args);
	}

	/**
	 * Validator bean for validating command line parameters
	 */
	@Bean
	public Validator configurationPropertiesValidator() {
		return new ConfigurationValidator();
	}
}
