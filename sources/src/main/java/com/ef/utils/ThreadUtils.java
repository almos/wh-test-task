package com.ef.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreadUtils
{
	public static void waitCompletion(ExecutorService service)
	{
		service.shutdown();
		
		while (true)
		{
			try {
				if (service.awaitTermination(1, TimeUnit.SECONDS)) 
					break;
			} catch (InterruptedException e) {
				break;
			}
		}
	}
}
