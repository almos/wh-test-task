package com.ef.utils;

import java.util.regex.Pattern;

public class NetworkUtils
{
	private static final String IPV4_PATTERN = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
	private static Pattern VALID_IPV4_PATTERN = Pattern.compile(IPV4_PATTERN, Pattern.CASE_INSENSITIVE);
	
	public static long ipToLong(String ip) 
	{
		String[] addrArray = ip.split("\\.");

		long num = 0;
		for (int i = 0; i < addrArray.length; i++) {
			int power = 3 - i;
			num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power)));
		}

		return num;
	}
	
	public static String longToIp(long ip)
	{
		StringBuilder result = new StringBuilder(15);

		for (int i = 0; i < 4; i++)
		{
			result.insert(0, Long.toString(ip & 0xff));
			if (i < 3) {
				result.insert(0, '.');
			}
			ip = ip >> 8;
		}
		
		return result.toString();
	}
	
	public static boolean isValidIp(String ipAddress) {
		return VALID_IPV4_PATTERN.matcher(ipAddress).matches();
	}
}
