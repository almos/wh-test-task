package com.ef.utils;

public class NumericUtils
{
	public static boolean isValidInt(String number)
	{
		try {
			parseInt(number);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static int parseInt(String number)
	{
		try {
			return Integer.parseInt(number); 
		} catch (NumberFormatException e) {
			throw e;
		}
	}
}
