package com.ef.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileUtils
{
	public static BufferedReader createBufferedReader(File f)
	{
		try {
			return Files.newBufferedReader(f.toPath());
		} catch (IOException e) {
			throw new RuntimeException("Unable to read file " + f.getAbsolutePath(), e);
		}
	}
	
	public static void closeQuitely(Closeable closeable)
	{
		try {
			closeable.close();
		} catch (Exception e) {
			// no op
		}
	}
}
