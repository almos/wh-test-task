package com.ef.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateUtils
{
	public static boolean isValid(String dateFormat, String date)
	{
		try {
			DateTimeFormatter.ofPattern(dateFormat).parse(date);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static LocalDateTime parseDate(String dateFormat, String date)
	{
		try {
			return LocalDateTime.from(
					DateTimeFormatter.ofPattern(dateFormat).parse(date));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static long toMillis(LocalDateTime dt) {
		return dt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
	}
	
	public static Timestamp toTimestamp(LocalDateTime dt) {
		return new Timestamp(DateUtils.toMillis(dt));
	}
	
	public static String format(LocalDateTime dt, String format) {
		return dt.format(DateTimeFormatter.ofPattern(format));
	}
}
