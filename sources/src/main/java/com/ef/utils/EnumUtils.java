package com.ef.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EnumUtils
{
	public static <E extends Enum<E>> E findByName(Class<E> ec, String name)
	{
		return Arrays.asList(ec.getEnumConstants())
				.stream()
				.filter(e -> e.name().equalsIgnoreCase(name))
				.findFirst()
				.orElse(null);
	}

	public static <E extends Enum<E>> List<String> getNames(Class<E> ec)
	{
		return Arrays.asList(ec.getEnumConstants())
				.stream()
				.map(e -> e.toString().toLowerCase())
				.collect(Collectors.toList());
	}
}
