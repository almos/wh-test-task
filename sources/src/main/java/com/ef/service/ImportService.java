package com.ef.service;

import java.util.List;

import com.ef.dal.entities.ImportMetadataEntity;
import com.ef.dal.entities.ThresholdingIpEntity;

/**
 * Defines high level semantics for data import 
 */
public interface ImportService
{
	/**
	 * Import source data into destination storage
	 * @return Import identifier
	 */
	ImportMetadataEntity importData();
	
	/**
	 * Find IPs that exceeds given threshold
	 * and insert them into separate destination/table for further analysis
	 * @param entry Import identifier
	 * @return
	 */
	List<ThresholdingIpEntity> trackThresholds(ImportMetadataEntity entry); 
}
