package com.ef.service;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.ef.dal.entities.ImportMetadataEntity;

/**
 * Triggers import logic when application is started from command line 
 */
@Service
@Profile("main")
public class CommandLineRunnerService
{
	@Autowired
	private ImportService importService;
	
	@PostConstruct
	public void start()
	{
		ImportMetadataEntity metadata = importService.importData();
		importService.trackThresholds(metadata)
			.stream()
			.map(e -> e.getIp())
			.forEach(System.out::println);
	}
}
