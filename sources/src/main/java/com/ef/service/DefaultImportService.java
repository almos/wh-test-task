package com.ef.service;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ef.configuration.ApplicationConfiguration;
import com.ef.dal.DestinationAccessLogDAL;
import com.ef.dal.ImportMetadataDAL;
import com.ef.dal.SourceAccessLogDAL;
import com.ef.dal.ThresholdingIpDAL;
import com.ef.dal.entities.AccessLogEntity;
import com.ef.dal.entities.ImportMetadataEntity;
import com.ef.dal.entities.ThresholdingIpEntity;
import com.ef.utils.ThreadUtils;

@Service
public class DefaultImportService implements ImportService
{
	@Autowired
	private SourceAccessLogDAL srcAccessLogDal;
	
	@Autowired
	private ApplicationConfiguration config;
	
	@Autowired
	private DestinationAccessLogDAL dstAccessLogDal;
	
	@Autowired
	private ImportMetadataDAL importMetadataDal;
	
	@Autowired
	private ThresholdingIpDAL thresholdingIpDal;
	
	@Override
	@Transactional
	public ImportMetadataEntity importData()
	{
		Iterator<List<AccessLogEntity>> batchIterator = srcAccessLogDal.batchIterator(
				config.getImportBatchSize());
		
		ExecutorService pool = Executors.newFixedThreadPool(
				config.getImportPoolSize());
		
		// getting import ID
		ImportMetadataEntity metadata = new ImportMetadataEntity(config.getAccessLog().getAbsolutePath());
		importMetadataDal.insert(metadata);
		
		// submit chunks to be inserted
		while (batchIterator.hasNext()) {
			List<AccessLogEntity> next = batchIterator.next();
			pool.execute(() -> { dstAccessLogDal.insert(next, metadata.getImportId()); });
		}

		// wait all threads
		ThreadUtils.waitCompletion(pool);
		
		return metadata;
	}

	@Override
	@Transactional
	public List<ThresholdingIpEntity> trackThresholds(ImportMetadataEntity entry)
	{
		Map<String, Long> thresholdingIps = dstAccessLogDal.find(config.getStartDate(),
				config.getEndDate(), config.getThreshold(), entry.getImportId());
		
		List<ThresholdingIpEntity> result = thresholdingIps
			.keySet()
			.stream()
			.map(k -> 
				new ThresholdingIpEntity()
					.setIp(k)
					.setCount(thresholdingIps.get(k))
					.setStartDate(config.getStartDate())
					.setEndDate(config.getEndDate())
					.setImportId(entry.getImportId())
					.setThreshold(config.getThreshold())
					.setReason(String.format("IP exceeded access threshold rate "
							+ "of %d by %d between %s and %s", 
							config.getThreshold(), (thresholdingIps.get(k) - config.getThreshold()), 
							config.getStartDate(), config.getEndDate())))
			.collect(Collectors.toList());
		
		thresholdingIpDal.insert(result);
		
		return result;
	}
}
