package com.ef.mapper;

import com.ef.dal.entities.AccessLogEntity;

/**
 * Defines interface of a mapper
 * to map war source data to a entity that 
 * could be used with destination storage layer
 */
public interface AccessLogEntryMapper
{
	/**
	 * Maps source data to AccessLogEntity
	 * @param fields
	 * @return
	 */
	AccessLogEntity map(String[] fields);
}
