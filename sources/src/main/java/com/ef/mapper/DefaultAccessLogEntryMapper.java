package com.ef.mapper;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ef.dal.entities.AccessLogEntity;
import com.ef.utils.DateUtils;
import com.ef.utils.NetworkUtils;
import com.ef.utils.NumericUtils;

import static com.ef.utils.DateUtils.parseDate;
import static com.ef.utils.DateUtils.toMillis;
import static com.ef.utils.NetworkUtils.ipToLong;
import static com.ef.utils.NumericUtils.parseInt;

@Component
public class DefaultAccessLogEntryMapper implements AccessLogEntryMapper
{
	/**
	 * Date/time format of access log entries
	 */
	private static final String REQUEST_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	
	/**
	 * Logger
	 */
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public AccessLogEntity map(String[] fields)
	{
		if (fields == null)
			return null;
		
		if (fields.length < 5)
			return null;
		
		String rawDateTime = fields[0],
			   rawIp = fields[1],
			   rawRequestLine = fields[2],
			   rawResponseCode = fields[3],
			   rawUserAgent = fields[4];
		
		if (DateUtils.isValid(REQUEST_DATE_TIME_FORMAT, rawDateTime) == false) {
			log.warn("{} doesn't match the format {}. Record {} will be ignored.", 
					rawDateTime, REQUEST_DATE_TIME_FORMAT, Arrays.asList(fields));
			return null;
		}
		
		if (NumericUtils.isValidInt(rawResponseCode) == false ||
				NumericUtils.parseInt(rawResponseCode) <= 0) {
			log.warn("{} is not a valid response code. Record {} will be ignored. ", 
					rawResponseCode, Arrays.asList(fields));
			return null;
		}
		
		if (NetworkUtils.isValidIp(rawIp) == false) {
			log.warn("{} is not a valid IPv4 address. Record {} will be ignored. ", 
					rawIp, Arrays.asList(fields));
			return null;
		}
		
		AccessLogEntity entry = new AccessLogEntity();
		entry.setRequestTs(toMillis(parseDate(REQUEST_DATE_TIME_FORMAT, rawDateTime)));
		entry.setSourceIp(ipToLong(rawIp));
		entry.setResponseCode(parseInt(rawResponseCode));
		entry.setUserAgent(rawUserAgent);
		entry.setSourceIp(ipToLong(rawIp));
		entry.setRequestLine(rawRequestLine);
		
		return entry;
	}
}
