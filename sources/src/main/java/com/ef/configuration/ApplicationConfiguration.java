package com.ef.configuration;

import java.io.File;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import com.ef.utils.DateUtils;
import com.ef.utils.EnumUtils;

@Component
@Validated
@ConfigurationProperties
public class ApplicationConfiguration
{
	/**
	 * Raw configuration properties from command line
	 */
	@Value("${startDate}")
	private String rawStartDate;
	
	@Value("${duration}")
	private String rawDuration;
	
	@Value("${accesslog}")
	private String rawAccessLogPath;
	
	@Value("${threshold}")
	private String rawThreshold;
	
	/**
	 * Processed/transformed properties from their raw counterparts
	 */
	private Integer threshold;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private DurationPeriod duration;
	private File accessLog;
	
	/**
	 * Import related configuration parameters 
	 */
	@Value("${import.batchSize:512}")
	private Integer importBatchSize;
	
	@Value("${import.poolSize:10}")
	private Integer importPoolSize;
	
	@Value("${import.source.separator:|}")
	private char sourceDataSeparator;
	
	@Value("${import.source.quote:\"}")
	private char sourceDataQuote;

	/**
	 * Format of start date passed as command line parameter
	 */
	public static final String START_DATE_FORMAT = "yyyy-MM-dd.HH:mm:ss";
	
	public enum DurationPeriod
	{
		DAILY(1, ChronoUnit.DAYS), HOURLY(1, ChronoUnit.HOURS);
		
		private int amount;
		private TemporalUnit unit;
		private DurationPeriod(int amount, TemporalUnit unit) {
			this.amount = amount;
			this.unit = unit;
		}
		
		public LocalDateTime nextDateTime(LocalDateTime dt) {
			return dt.plus(amount, unit);
		}
	}
	
	/**
	 * This method runs after validation completed, so assuming here that
	 * all values of input parameters are verifier
	 */
	@PostConstruct
	public void init()
	{
		threshold = Integer.parseInt(this.getRawThreshold());
		duration = EnumUtils.findByName(DurationPeriod.class, getRawDuration());
		startDate = DateUtils.parseDate(START_DATE_FORMAT, getRawStartDate());
		endDate = duration.nextDateTime(startDate);
		accessLog = new File(getRawAccessLogPath());
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public DurationPeriod getDuration() {
		return duration;
	}

	public File getAccessLog() {
		return accessLog;
	}
	
	public Integer getThreshold() {
		return threshold;
	}

	public Integer getImportBatchSize() {
		return importBatchSize;
	}

	public Integer getImportPoolSize() {
		return importPoolSize;
	}

	public char getSourceDataSeparator() {
		return sourceDataSeparator;
	}

	public char getSourceDataQuote() {
		return sourceDataQuote;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	protected String getRawStartDate() {
		return rawStartDate;
	}

	protected void setRawStartDate(String rawStartDate) {
		this.rawStartDate = rawStartDate;
	}

	protected String getRawDuration() {
		return rawDuration;
	}

	protected void setRawDuration(String rawDuration) {
		this.rawDuration = rawDuration;
	}

	protected String getRawAccessLogPath() {
		return rawAccessLogPath;
	}

	protected void setRawAccessLogPath(String rawAccessLogPath) {
		this.rawAccessLogPath = rawAccessLogPath;
	}

	protected String getRawThreshold() {
		return rawThreshold;
	}

	protected void setRawThreshold(String rawThreshold) {
		this.rawThreshold = rawThreshold;
	}
}
