package com.ef.configuration;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.ef.configuration.ApplicationConfiguration.DurationPeriod;
import com.ef.utils.DateUtils;
import com.ef.utils.EnumUtils;

public class ConfigurationValidator implements Validator
{
	@Override
	public boolean supports(Class<?> type) {
		return type == ApplicationConfiguration.class;
	}

	@Override
	public void validate(Object o, Errors e)
	{
		ApplicationConfiguration c = (ApplicationConfiguration) o;
		
		validateDuration(c.getRawDuration(), e);
		validateThreshold(c.getRawThreshold(), e);
		validateStartDate(c.getRawStartDate(), e);
		validateAccessLogPath(c.getRawAccessLogPath(), e);
	}
	
	private void validateThreshold(String threshold, Errors e)
	{
		if (threshold == null) {
			e.reject("threshold", "Threshold may not be empty");
			return;
		}
		
		int t = 0;
		try {
			t = Integer.parseInt(threshold);
		} catch (Exception e1) {
			e.reject("threshold", "Threshold value is invalid");
			return;
		}
		
		if (t <= 0)
			e.reject("threshold", "Threshold must be greater than 0");
	}
	
	private void validateDuration(String duration, Errors e)
	{
		if (duration == null || duration.equals("")) {
			e.reject("duration", "Duration may not be empty");
			return;
		}
		
		if (EnumUtils.findByName(DurationPeriod.class, duration) == null)
			e.reject("duration", "Supplied duration is invalid and not supported. "
					+ "Supported values: " + EnumUtils.getNames(DurationPeriod.class));
	}
	
	private void validateStartDate(String startDate, Errors e)
	{
		if (startDate == null || startDate.equals("")) {
			e.reject("startDate", "Start date may not be empty");
			return;
		}
		
		boolean isValidDate = DateUtils.isValid(ApplicationConfiguration.START_DATE_FORMAT, startDate);
		if (!isValidDate)
			e.reject("startDate", "Start date is invalid");
	}
	
	private void validateAccessLogPath(String accessLogPath, Errors e)
	{
		if (accessLogPath == null || accessLogPath.equals("")) {
			e.reject("accessLogPath", "Path to access log may not be empty");
			return;
		}
		
		Path path = Paths.get(accessLogPath);
		
		if (Files.notExists(path)) {
			e.reject("accessLogPath", "Specified access log file doesn't exist");
			return;
		}
		
		if (!Files.isReadable(path))
			e.reject("accessLogPath", "Specified access log file isn't readable. Please check file permissions");
	}
}
