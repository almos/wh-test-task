-- import metadata
CREATE TABLE IF NOT EXISTS imports (
	i_id bigint(10) NOT NULL auto_increment,
	i_ts datetime NOT NULL,
	i_file varchar(512) NOT NULL,
	PRIMARY KEY(i_id)
) ENGINE=InnoDB;

-- actual data
CREATE TABLE IF NOT EXISTS access_log (
	al_id bigint(10) NOT NULL auto_increment,
	al_ts bigint(10) NOT NULL,
	al_ip bigint(10) NOT NULL,
	al_rc smallint(3) NOT NULL,
	al_rl varchar(512) NOT NULL,
	al_ua varchar(512) NOT NULL,
	al_i_id bigint(10) NOT NULL,
	PRIMARY KEY(al_id),
	INDEX idx_al_iid (al_i_id, al_ts)
) ENGINE=InnoDB;

-- high hit rate
CREATE TABLE IF NOT EXISTS hits (
	h_id bigint(10) NOT NULL auto_increment,
	h_i_id bigint(10) NOT NULL,
	h_ip varchar(15) NOT NULL,
	h_count int(10) NOT NULL,
	h_threshold int(10) NOT NULL,
	h_start datetime NOT NULL,
	h_end datetime NOT NULL,
	h_reason varchar(128) NOT NULL,
	PRIMARY KEY(h_id),
	INDEX idx_h_ip(h_ip)
) ENGINE=InnoDB;
