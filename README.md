# wh-test-task

### Version
0.0.1

### File system layout

Project is delivered with following directories:

* [bin] - compiled solution code
* [data] - data set used for testing the solution
* [sources] - project sources
* [sql] - database schema

### Background

Application is built using Spring Boot framework.
It uses MySQL as a storage backend and org.h2.Driver in MySQL emulation mode for unit testing.

### Prerequisities

* Git
* MySQL 5.5
* Apache Maven 3.2
* JDK 1.8

### Build process

Clone repository

```sh
$ git clone https://bitbucket.org/almos/wh-test-task.git
```

Build project

```sh
$ cd wh-test-task/sources
$ mvn clean package
```

### Set up basic configuration

Adjust configuration (sources/src/main/resources/application.properties)
It could be externalized as per Spring boot externalization rules.

| Property name        | Data type           | Description  |
| ------------- |:-------------:| -----:|
| spring.datasource.url	| String | Provider specific JDBC URL |
| spring.datasource.username | String | Username to connect to database specified by spring.datasource.url |
| spring.datasource.password | String | Password of user specified by spring.datasource.username to connect to database specified by spring.datasource.url |
| import.batchSize | int | Size of batch of access log entries to import to database at a time |
| import.poolSize | int | Number of worker threads to import to database | 
| import.source.separator | char | Input data separator |
| import.source.quote | char | Quotation character used in input data (data quoted with this char will not be looked up for import.source.separator within) |

### Run process

Application is Spring Boot based and uses application.properties for configuration of datasource and logging.
Runtime configuration could be defined using command line parameters listed below

| Property name        | Data type           | Description  |
| ------------- |:-------------:| -----:|
| accesslog | String | Path to access log file |
| startDate | String | Date starting from which imported entries should be checked for IPs that are exceeding specified threshold | 
| duration | String | Interval starting from startDate to check thresholding IPs |
| threshold | int | Threshold value for IPs address interactions |

```sh
$ cp target/parser-0.0.1-SNAPSHOT.jar ./bin/parser.jar
```

Run application
```sh
$ cd bin
$ java -jar parser.jar --accesslog=../data/access.log --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 
```

Application will automatically deploy schema from sources/src/main/resources/schema.sql to the datasource specified by spring.datasource.* parameters

Run unit tests
```sh
$ cd sources
$ mvn test
...
Results :

Tests run: 17, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 35.592 s
[INFO] Finished at: 2017-10-28T14:01:35+03:00
[INFO] Final Memory: 27M/324M
[INFO] ------------------------------------------------------------------------
```

Application test examples

```sh
$ java -jar parser.jar --accesslog=../data/access.log --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100
192.168.77.101
192.168.228.188

$ java -jar parser.jar --accesslog=../data/access.log --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500
192.168.203.111
192.168.51.205
192.168.129.191
192.168.185.164
192.168.31.26
192.168.52.153
192.168.33.16
192.168.162.248
192.168.38.77
192.168.62.176
192.168.199.209
192.168.219.10
192.168.102.136
192.168.143.177
192.168.206.141
```

### SQL queries

(1) Write MySQL query to find IPs that mode more than a certain number of requests for a given time period. Ex: Write SQL to find IPs that made more than 100 requests starting from 2017-01-01.13:00:00 to 2017-01-01.14:00:00.

```sql
SELECT INET_NTOA(al_ip), COUNT(*)
FROM `access_log`
WHERE al_i_id = 1
AND (al_ts BETWEEN 
	UNIX_TIMESTAMP(STR_TO_DATE('2017-01-01.13:00:00', '%Y-%m-%d.%H:%i:%s'))*1000
	AND
	UNIX_TIMESTAMP(STR_TO_DATE('2017-01-01.14:00:00', '%Y-%m-%d.%H:%i:%s'))*1000
) 
GROUP BY al_ip 
HAVING COUNT(*) > 100
```

(2) Write MySQL query to find requests made by a given IP.

```sql
SELECT al_rl
FROM `access_log` 
WHERE al_i_id = 1               
AND al_ip = INET_ATON("192.168.143.177");
```

For this kind of query it would be recommended to add one more index on (al_i_id, al_ip)
